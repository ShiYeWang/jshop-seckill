package com.sugo.seckill.order.service;

import com.sugo.seckill.error.BaseException;
import com.sugo.seckill.http.HttpResult;
import com.sugo.seckill.page.PageResult;
import com.sugo.seckill.pojo.*;

/**
 * 服务层接口
 * @author Administrator
 */
public interface SeckillOrderService {

	/**
	 * 返回分页列表
	 * @return
	 */
	public PageResult findPage(int pageNum, int pageSize);
	

	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	public TbSeckillOrder findOne(Long id);

	public void updateOrderStatus(String out_trade_no, String transaction_id);


	public TbPayLog searchPayLogFromRedis(String userId);


	/**
	 * @Description: 从redis中查询用户信息
	 * @Author: hubin
	 * @CreateDate: 2020/6/10 10:47
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/6/10 10:47
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	FrontUser getUserInfoFromRedis(String token);

    HttpResult getOrderMoney(Long orderId);

	TbSeckillOrder findOrderById(Long orderId);

	public void getSeckillGoods(Long seckillId);




	/**
	 * @Description: 普通下单操作
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilled(Long killId,String userId);



	/**
	 * @Description: MySQL分布式锁，forupdate 实现悲观锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledLockByForUpdate(Long killId,String userId);


	/**
	 * @Description: MySQL分布式锁，version版本的方式 实现乐观锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledLockByForVersion(Long killId,String userId);


	/**
	 * @Description: Redis分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledLockByForRedis(Long killId,String userId);

	/**
	 * @Description: zookeeper分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledLockByForZK(Long killId,String userId);


	/**
	 * @Description: 对下单操作进行性能优化的改造
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledByBestOperation(Long killId,String userId);
	/**
	 * @Description: 执行下单操作
	 * @Author: hubin
	 * @CreateDate: 2021/3/25 23:00
	 * @UpdateUser: hubin
	 * @UpdateDate: 2021/3/25 23:00
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	void startAsyncKilled(TbSeckillOrder kill);

	/**
	 * @Description: 下单操作
	 * @Author: hubin
	 * @CreateDate: 2021/3/27 21:25
	 * @UpdateUser: hubin
	 * @UpdateDate: 2021/3/27 21:25
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	HttpResult startSubmitOrderByRedis(long killId, String userId) throws BaseException;
}
