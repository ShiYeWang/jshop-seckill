package com.sugo.seckill.order.service.impl;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.github.pagehelper.Page;

import com.sugo.seckill.aop.lock.ServiceLock;
import com.sugo.seckill.aop.redis.ServiceRedisLock;
import com.sugo.seckill.aop.zk.ServiceZkLock;
import com.sugo.seckill.error.BaseException;
import com.sugo.seckill.http.HttpResult;
import com.sugo.seckill.http.HttpStatus;
import com.sugo.seckill.lock.distributedlock.redis.RedissLockUtil;
import com.sugo.seckill.mapper.order.SeckillGoodsMapper;
import com.sugo.seckill.mapper.order.SeckillOrderMapper;
import com.sugo.seckill.mapper.pay.TbPayLogMapper;
import com.sugo.seckill.order.service.SeckillOrderService;
import com.sugo.seckill.page.PageResult;
import com.sugo.seckill.pojo.*;
import com.sugo.seckill.queue.jvm.SeckillQueue;
import com.sugo.seckill.queue.mq.MqProducer;
import com.sugo.seckill.utils.IdWorker;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import com.github.pagehelper.PageHelper;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import tk.mybatis.mapper.entity.Example;

/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class SeckillOrderServiceImpl implements SeckillOrderService {

	@Autowired
	private SeckillOrderMapper seckillOrderMapper;

	//注入支付
	@Autowired
	private TbPayLogMapper payLogMapper;


	//注入商品对象
	@Autowired
	private SeckillGoodsMapper seckillGoodsMapper;

	//注入redis
	@Autowired
	private RedisTemplate redisTemplate;

	//注入Idworker
	@Autowired
	private IdWorker idWorker;

	@Autowired
	private MqProducer producer;



	//程序锁
	//互斥锁 参数默认false，不公平锁
	private Lock lock = new ReentrantLock(true);

	//日志
	private final static Logger LOGGER = LoggerFactory.getLogger(SeckillOrderServiceImpl.class);



	// disruptor 队列 --- 600w /s
	// 分布式队列
	// redis
	// mq


	public void getSeckillGoods(Long seckillId){

		redisTemplate.delete("seckill_goods_"+seckillId);
		redisTemplate.delete("seckill_goods_stock_"+seckillId);

		//创建example对象
		Example example = new Example(TbSeckillGoods.class);
		Example.Criteria criteria = example.createCriteria();

		/*//设置查询条件
		//状态可用
		criteria.andEqualTo("status",1);
		//时间必须在活动区间
		criteria.andCondition("now() BETWEEN start_time_date AND end_time_date");
		//库存必须大于0
		criteria.andGreaterThan("stockCount",0);
		criteria.andEqualTo("id",1);

		//获取Redis中所有的key,实现排除当前Redis中已经存在的商品
		Set<Long> ids = redisTemplate.boundHashOps("seckillGoods").keys();
		//判断
		if(ids!=null && ids.size()>0){
			criteria.andNotIn("id",ids);
		}

		//查询
		List<TbSeckillGoods> seckillGoodsList = seckillGoodsMapper.selectByExample(example);
*/
		TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(seckillId);

		//判断是否有入库商品
		//if(seckillGoodsList!=null && seckillGoodsList.size()>0){
			//循环
			//for (TbSeckillGoods goods : seckillGoodsList) {

				//放入商品对象
				redisTemplate.opsForValue().set("seckill_goods_"+seckillId,seckillGoods);
				//存储库存
				redisTemplate.opsForValue().set("seckill_goods_stock_"+seckillId,seckillGoods.getStockCount());

               /* //存储到redis
                redisTemplate.boundHashOps("seckillGoods").put(String.valueOf(goods.getId()),goods);
                //存储库存:剩余库存存，用来防止超卖
                redisTemplate.boundHashOps("seckillGoodsCount").put(String.valueOf(goods.getId()),goods.getStockCount());*/

			//}
		//}
	}

	/**
	 * @Description: 普通下单操作
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceLock
	@Override
	public HttpResult startKilled(Long killId, String userId) {

		try {
			//实现一个加锁的动作

			//lock.lock();

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
                return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
            }
			if(seckillGoods.getStatus() != 1){
                return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
            }
			if(seckillGoods.getStockCount() <= 0){
                return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
            }
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
                return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
            }
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
                return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
            }

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally {
			lock.unlock();
		}*/
		return null;
	}

	/**
	 * @Description: MySQL分布式锁，forupdate 实现悲观锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@Override
	public HttpResult startKilledLockByForUpdate(Long killId, String userId) {
		try {

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKeyBySQLLock(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: MySQL分布式锁，version版本的方式 实现乐观锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@Override
	public HttpResult startKilledLockByForVersion(Long killId, String userId) {
		try {

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//乐观锁更新库存，更新版本
			int count = seckillGoodsMapper.updateSeckillGoodsByPrimaryKeyByVersion(killId, seckillGoods.getVersion());

			// 如果修改失败
			if(count != 1){
				return  HttpResult.error("下单失败");
			}


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: Redis分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceRedisLock
	@Override
	public HttpResult startKilledLockByForRedis(Long killId, String userId) {
		try {

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: zk分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceZkLock
	@Override
	public HttpResult startKilledLockByForZK(Long killId, String userId) {
		try {
			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: 对下单操作进行性能优化的改造
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public HttpResult startKilledByBestOperation(Long killId, String userId) {
		try {

			// 判断库存表示，是否售罄
			Integer end = (Integer) redisTemplate.opsForValue().get("seckill_goods_end_" + killId);
			if(end!=null && end == 0){
				return HttpResult.error("商品已售罄");
			}

			// 从数据库查询商品数据
			// 优化一： 商品读换缓存
			TbSeckillGoods seckillGoods = (TbSeckillGoods) redisTemplate
								.opsForValue()
								.get("seckill_goods_"+killId);

			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}



			// // 优化二： 扣减库存缓存化
			boolean result = this.producerStockCount(killId);
			if(!result){
				return HttpResult.error("下单失败");
			}


			// 优化三：下单异步化：队列 BlockingQueue,Disruptor,RocketMQ
			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			// 生产队列
			Boolean produce = SeckillQueue.getMailQueue().produce(order);
			if(!produce){
				return HttpResult.error("下单失败");
			}

			//seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @Description: 执行下单操作
	 * @Author: hubin
	 * @CreateDate: 2021/3/25 23:00
	 * @UpdateUser: hubin
	 * @UpdateDate: 2021/3/25 23:00
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public void startAsyncKilled(TbSeckillOrder kill) {
		// 异步把订单写入数据库
		seckillOrderMapper.insertSelective(kill);
	}

	/**
	 * @Description: 事务消息，解决数据最终一致性
	 * @Author: hubin
	 * @CreateDate: 2021/3/27 21:26
	 * @UpdateUser: hubin
	 * @UpdateDate: 2021/3/27 21:26
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public HttpResult startSubmitOrderByRedis(long killId, String userId) throws BaseException {
		try {

			// 判断库存表示，是否售罄
			Integer end = (Integer) redisTemplate.opsForValue().get("seckill_goods_end_" + killId);
			if(end!=null && end == 0){
				return HttpResult.error("商品已售罄");
			}

			// 从数据库查询商品数据
			// 优化一： 商品读换缓存
			TbSeckillGoods seckillGoods = (TbSeckillGoods) redisTemplate
					.opsForValue()
					.get("seckill_goods_"+killId);

			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}



			// // 优化二： 扣减库存缓存化
			boolean result = this.reduceStockCount(killId);
			if(!result){
				// 设置一个异常，baseException,此异常让事务消息捕获
				throw new BaseException(HttpStatus.SEC_GOODS_STOCK_FAIL,"扣减库存失败");
			}


			// 优化三：下单异步化：队列 BlockingQueue,Disruptor,RocketMQ
			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			// 生产队列
			Boolean produce = SeckillQueue.getMailQueue().produce(order);
			if(!produce){
				throw new BaseException(HttpStatus.SC_PRECONDITION_FAILED,"下单失败");
			}

			//seckillOrderMapper.insertSelective(order);

			// 以上业务都OK，事务提交状态
			seckillGoods.setTransactionStatus(1);
			seckillGoods.setStockCount(null);

			// 更新事务状态
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();

			throw new BaseException(HttpStatus.SC_EXPECTATION_FAILED,"下单异常");

		}

	}

	private boolean reduceStockCount(long killId) {

		// 优化二： 扣减库存缓存化
		Long result = redisTemplate.opsForValue().increment("seckill_goods_stock_" + killId, -1);
		// 判断
		if(result>0){
			// 表示库存扣减成功
			return true;
		}else if(result == 0){
			// 表示库存扣减成功
			// 添加一个标识
			redisTemplate.opsForValue().set("seckill_goods_end_"+killId,0);
			return true;
		}
		return false;
	}

	/**
	 * @Description: 库存缓存化扣减
	 * @Author: hubin
	 * @CreateDate: 2021/3/25 22:19
	 * @UpdateUser: hubin
	 * @UpdateDate: 2021/3/25 22:19
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	private boolean producerStockCount(Long killId) {
		// 优化二： 扣减库存缓存化
		Long result = redisTemplate.opsForValue().increment("seckill_goods_stock_" + killId, -1);
		// 判断
		if(result>0){
			// 表示库存扣减成功
			boolean res = producer.asncSendMsg(killId);
			if(!res){
				// 库存回补
				redisTemplate.opsForValue().increment("seckill_goods_stock_" + killId, 1);
			}
			return true;
		}else if(result == 0){

			// 表示库存扣减成功
			boolean res = producer.asncSendMsg(killId);
			if(!res){
				// 库存回补
				redisTemplate.opsForValue().increment("seckill_goods_stock_" + killId, 1);
			}
			// 添加一个标识
			redisTemplate.opsForValue().set("seckill_goods_end_"+killId,0);
			return true;
		}
		return false;
	}


	/**
	 * 按分页查询
	 */
	@Override
	@Transactional
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		Page<TbSeckillOrder> page=   (Page<TbSeckillOrder>) seckillOrderMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}


	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	@Transactional
	public TbSeckillOrder findOne(Long id){
		return seckillOrderMapper.selectByPrimaryKey(id);
	}

	/**
	 * @Description: 支付完毕，更新订单的状态
	 * @Author: hubin
	 * @CreateDate: 2020/6/10 22:20
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/6/10 22:20
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	@Transactional
	public void updateOrderStatus(String out_trade_no, String transaction_id) {
		//1.修改支付日志状态

		TbPayLog payLog = payLogMapper.selectByPrimaryKey(out_trade_no);
		payLog.setPayTime(new Date());//支付时间
		payLog.setTradeState("1");//交易状态
		payLog.setTransactionId(transaction_id);//流水号
		payLogMapper.updateByPrimaryKey(payLog);

		//2.修改订单状态
		String orderList = payLog.getOrderList();
		String[] ids = orderList.split(",");//订单号
		for(String id:ids){

			TbSeckillOrder order = seckillOrderMapper.selectByPrimaryKey( Long.valueOf(id) );
			order.setStatus("2");//支付状态
			order.setPayTime(new Date());//支付时间
			seckillOrderMapper.updateByPrimaryKey(order);
		}
	}





	@Override
	public TbPayLog searchPayLogFromRedis(String userId) {
		return (TbPayLog) redisTemplate.boundHashOps("payLog").get(userId);
	}

	/**
	 * @Description: 根据token查询用户信息
	 * @Author: hubin
	 * @CreateDate: 2020/6/10 10:47
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/6/10 10:47
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public FrontUser getUserInfoFromRedis(String token) {
		FrontUser user =
				(FrontUser) redisTemplate.opsForValue().get(token);
		return user;
	}

	@Override
	public HttpResult getOrderMoney(Long orderId) {
		TbSeckillOrder order = seckillOrderMapper.selectByPrimaryKey(orderId);
		HttpResult httpResult = new HttpResult();
		httpResult.setData(order.getMoney());
		return httpResult;
	}

	@Override
	public TbSeckillOrder findOrderById(Long orderId) {
		return seckillOrderMapper.selectByPrimaryKey(orderId);
	}
}
