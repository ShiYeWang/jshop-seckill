package com.sugo.seckill.aop.redis;

import java.lang.annotation.*;

/**
 * @ClassName 自定义注解，实现AOP锁
 * @Description
 * @Author hubin
 * @Date 2021/3/25 20:10
 * @Version V1.0
 **/
@Target({ElementType.PARAMETER,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceRedisLock {

    String description() default "";

}
