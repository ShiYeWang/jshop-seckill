package com.sugo.seckill.web.order;

import com.sugo.seckill.http.HttpResult;
import com.sugo.seckill.http.HttpStatus;
import com.sugo.seckill.order.service.SeckillOrderService;
import com.sugo.seckill.pojo.FrontUser;
import com.sugo.seckill.queue.mq.MqProducer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * controller
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/seckill")
public class SeckillOrderController {

	@Autowired
	private SeckillOrderService seckillOrderService;

	/*@Autowired
	private MqProducer producer;*/


	// 线程池，流量泄洪
/*	ExecutorService executorService = new ThreadPoolExecutor(4,
			20,
			100,
			TimeUnit.SECONDS,
			new ArrayBlockingQueue<Runnable>(2000));*/


			/**
             * @Description: 获取时间
             * @Author: hubin
             * @CreateDate: 2020/6/10 16:19
             * @UpdateUser: hubin
             * @UpdateDate: 2020/6/10 16:19
             * @UpdateRemark: 修改内容
             * @Version: 1.0
             */
	@RequestMapping("/submitOrder/times")
	public HttpResult getConcurrentTime(){
		return HttpResult.ok(System.currentTimeMillis()+"");
	}


	@RequestMapping("/test")
	public HttpResult getSeckillGoods(Long seckillId){
		seckillOrderService.getSeckillGoods(seckillId);
		return HttpResult.ok();
	}

	/**
	 * @Description: 普通下单操作
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/{killId}/{token}")
	public HttpResult startKilled(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilled(killId, userId);

		return result;

	}


/*
	*//**
	 * @Description: MySQL分布式锁，forupdate 实现悲观锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 *//*
	@RequestMapping("/order/kill/forupdate/{killId}/{token}")
	public HttpResult startKilledLockByForUpdate(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledLockByForUpdate(killId,userId);

		return result;

	}

	*//**
	 * @Description: MySQL分布式锁，version版本的方式 实现乐观锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 *//*
	@RequestMapping("/order/kill/version/{killId}/{token}")
	public HttpResult startKilledLockByForVersion(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledLockByForVersion(killId,userId);

		return result;

	}

	*//**
	 * @Description: Redis分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 *//*
	@RequestMapping("/order/kill/redis/{killId}/{token}")
	public HttpResult startKilledLockByForRedis(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledLockByForRedis(killId,userId);

		return result;

	}


	*//**
	 * @Description: zk分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 *//*
	@RequestMapping("/order/kill/zk/{killId}/{token}")
	public HttpResult startKilledLockByForZK(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledLockByForZK(killId,userId);

		return result;

	}

	*//**
	 * @Description: 对下单操作进行性能优化的改造
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 *//*
	@RequestMapping("/order/kill/best/{killId}/{token}")
	public HttpResult startKilledByBestOperation(@PathVariable Long killId, @PathVariable String token) throws BaseException{
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";

		// 发送消息，下单
		// 20个等等队列，流量泄洪；线程池线程同步的调用方法
		Future<Object> future = executorService.submit(new Callable<Object>() {
			@Override
			public Object call() throws Exception {

				// 发送事务消息
				boolean res = producer.asncSendTransactionMsg(killId, userId);

				if(!res){
					throw new BaseException(HttpStatus.SC_METHOD_FAILURE,"消息发送失败");
				}
				return null;
			}
		});

		// 同步阻塞发送
		try {
			future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new BaseException(HttpStatus.SC_METHOD_FAILURE,"消息发送失败");
		} catch (ExecutionException e) {
			e.printStackTrace();
			throw new BaseException(HttpStatus.SC_METHOD_FAILURE,"消息发送失败");
		}

		return HttpResult.ok("下单成功");

	}*/
}
